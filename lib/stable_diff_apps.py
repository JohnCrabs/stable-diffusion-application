import os
import torch

from itertools import repeat

from PIL import Image
from diffusers import DiffusionPipeline
from diffusers import StableDiffusionPipeline
from diffusers import StableDiffusionImg2ImgPipeline
from diffusers import StableDiffusionInpaintPipeline

class StableDiffusionApplications:
    def __init__(self, model_id: str = None, device: str = "cuda", use_safetensors=False):
        # self.auth_token = auth_token
        self.model_id = model_id
        self.device = "cpu" if device != "cuda" else ("cuda" if torch.cuda.is_available() else "cpu")
        self.generator = torch.Generator(self.device)
        self.use_safetensors = use_safetensors

        self.randomImage_generator = self.compile_RandomImage()
        self.text2image_generator = self.compile_text2image()
        self.img2img_generator = self.compile_img2img()
        self.inpaint_generator = self.compile_inpaint()

    @staticmethod
    def init_image(path: str, img_size: tuple = (512, 512)):
        img = Image.open(path).convert("RGB")
        img.resize(img_size)
        return img

    def set_model_id(self, new_model_id: str):
        self.model_id = new_model_id

    def set_device(self, new_device: str):
        self.device = "cpu" if device != "cuda" else ("cuda" if torch.cuda.is_available() else "cpu")
        self.generator = torch.Generator(self.device)

    def compile_RandomImage(self):
        pipeline = DiffusionPipeline.from_pretrained(
            "anton-l/ddpm-butterflies-128",
            use_safetensors=self.use_safetensors
        )
        return pipeline.to(self.device)

    def compile_text2image(self):
        pipeline = DiffusionPipeline.from_pretrained(
            "runwayml/stable-diffusion-v1-5",
            torch_dtype=torch.float16,
            use_safetensors=self.use_safetensors
        )
        return pipeline.to(self.device)

    def compile_img2img(self):
        pipeline = StableDiffusionImg2ImgPipeline.from_pretrained(
            "nitrosocke/Ghibli-Diffusion",
            torch_dtype=torch.float16,
            use_safetensors=self.use_safetensors
        )
        return pipeline.to(self.device)

    def compile_inpaint(self):
        # "andregn/Realistic_Vision_V3.0-inpainting"
        pipeline = StableDiffusionInpaintPipeline.from_pretrained(
            "runwayml/stable-diffusion-inpainting",
            torch_dtype=torch.float16,
            use_safetensors=self.use_safetensors,
            variant="fp16",
        )
        return pipeline.to(self.device)

    def randomImage(self):
        print("Randomized Image:")
        return self.randomImage_generator().images[0]

    def text2image(self, prompt:str):
        print("Text to Image:")
        return self.text2image_generator(prompt, generator=self.generator).images[0]

    def img2img(self, img, prompt:str or list[str] or None = None, n_prompt: str or list[str] or None = None, strength:float=0.75, guidance_scale:float=7.5):
        print("Image to Image:")
        return self.img2img_generator(image=img, prompt=prompt, negative_prompt=n_prompt, strength=strength, guidance_scale=guidance_scale, generator=self.generator).images[0]

    def image_inpainting(self, img, mask, prompt:str or list[str] or None = None):
        print("Image Inpainting:")
        return self.inpaint_generator(prompt=prompt, image=img, mask_image=mask).images[0]

    def mass_randomImage(self):
        pass

    def mass_text2image(self):
        pass

    def mass_img2img(self):
        pass

    def mass_image_inpainting(self, img, mask, iters: int, export_to: str, prompt:str or list[str] or None = None):
        if not os.path.exists(export_to):
            os.makedirs(export_to)
        num_zeros = str(iters).__len__()
        for __index__ in range(iters):
            fileName = "".join(repeat("0", num_zeros)) + ".png"
            print("Generating image: " + fileName)
            image = self.image_inpainting(
                img=img,
                mask=mask,
                prompt=prompt
            )
            export_path = os.path.normpath(export_to + "/" + fileName)
            print("Export image to: " + export_path)
            image.save(export_path)

if __name__=="__main__":
    text_prompt = "A Flower from Space, Rose, Blue-Lila, realistic, Ultra HD, 8K"

    # img_prompt = ["Nature Based Solutions", "Urban Planning",
    #      "Park", "Benches", "Fountain", "Trees",
    #      "Flowers", "Grass", "Greenery", "Health and Well-being"
    #      "Photo-Realistic", "Ultra HD", "8K"]
    img_prompt = "Nature Based Solutions, Urban Planning, Park, Benches, Fountain, Trees, Flowers, Grass, Greenery, Health and Well-being, Photo-Realistic, 0Ultra HD, 8K"
    img_path = "../img/Input/DilaveriCoast001.png"
    img_mask_path = "../img/Input/DilaveriCoast001_mask.png"
    export_img_path = "../img/Output/sample_00"
    iters = 10

    sda = StableDiffusionApplications(use_safetensors=False)
    # image = sda.image_inpainting(
    #     img=sda.init_image(path=img_path, img_size=(512, 512)),
    #     mask=sda.init_image(path=img_mask_path, img_size=(512, 512)),
    #     prompt=img_prompt
    # )
    sda.mass_image_inpainting(
        img=sda.init_image(path=img_path, img_size=(512, 512)),
        mask=sda.init_image(path=img_mask_path, img_size=(512, 512)),
        iters=iters,
        export_to=export_img_path,
        prompt=img_prompt
    )

